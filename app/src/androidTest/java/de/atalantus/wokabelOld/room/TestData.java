package de.atalantus.wokabelOld.room;

import java.util.Arrays;
import java.util.List;

import de.atalantus.wokabelOld.services.room.entities.SubjectEntity;
import de.atalantus.wokabelOld.services.room.entities.UnitEntity;
import de.atalantus.wokabelOld.services.room.entities.VocableEntity;

/**
 * Utility class that holds values to be used for testing.
 */
public class TestData {

    static final VocableEntity VOCABLE_ENTITY = new VocableEntity("V01", "present", "Geschenk, Gegenwart",
            "Fängt auch mit g an", 1,  "B01");

    static final VocableEntity VOCABLE_ENTITY2 = new VocableEntity("V02", "flor", "Blume, Blüte",
            "Ich kann kein Spanisch", 1, "B02");

    static final List<VocableEntity> VOCABLES = Arrays.asList(VOCABLE_ENTITY, VOCABLE_ENTITY2);



    static final UnitEntity UNIT_ENTITY = new UnitEntity("B01", "Unit 1", "P01");

    static final UnitEntity UNIT_ENTITY1 = new UnitEntity("B02", "Unit 1", "P02");

    static final List<UnitEntity> UNITS = Arrays.asList(UNIT_ENTITY, UNIT_ENTITY1);



    static final SubjectEntity SUBJECT_ENTITY = new SubjectEntity("P01", "Englisch", "englisch");

    static final SubjectEntity SUBJECT_ENTITY1 = new SubjectEntity("P02", "Spanisch", "spanisch");

    static final List<SubjectEntity> SUBJECTS = Arrays.asList(SUBJECT_ENTITY, SUBJECT_ENTITY1);
}
