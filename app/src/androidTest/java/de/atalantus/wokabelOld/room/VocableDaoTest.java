package de.atalantus.wokabelOld.room;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.stream.Collectors;

import de.atalantus.wokabelOld.LiveDataTestUtil;
import de.atalantus.wokabelOld.services.room.WokabelDatabase;
import de.atalantus.wokabelOld.services.room.daos.VocableDao;
import de.atalantus.wokabelOld.services.room.entities.VocableEntity;

import static de.atalantus.wokabelOld.room.TestData.SUBJECTS;
import static de.atalantus.wokabelOld.room.TestData.UNITS;
import static de.atalantus.wokabelOld.room.TestData.UNIT_ENTITY;
import static de.atalantus.wokabelOld.room.TestData.VOCABLES;
import static de.atalantus.wokabelOld.room.TestData.VOCABLE_ENTITY;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class VocableDaoTest {

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    private WokabelDatabase mDatabase;
    private VocableDao mVocableDao;

    @Before
    public void initDb() throws Exception
    {
        // using an in-memory database because the information stored here disappears when the
        // process is killed
        mDatabase = Room.inMemoryDatabaseBuilder(ApplicationProvider.getApplicationContext(),
                WokabelDatabase.class)
                // allowing main thread queries, just for testing
                .allowMainThreadQueries()
                .build();

        mVocableDao = mDatabase.vocableDao();
    }

    @After
    public void closeDb() throws Exception {
        mDatabase.close();
    }

    @Test
    public void getVocablesWhenNoVocablesInserted() throws InterruptedException {
        List<VocableEntity> vocables = LiveDataTestUtil.getValue(mVocableDao.loadAll());

        assertTrue(vocables.isEmpty());
    }

    @Test
    public void getVocablesAfterInserted() throws InterruptedException {
        mDatabase.subjectDao().insertAll(SUBJECTS);
        mDatabase.unitDao().insertAll(UNITS);

        mVocableDao.insertAll(VOCABLES);

        List<VocableEntity> vocables = LiveDataTestUtil.getValue(mVocableDao.loadAll());

        assertThat(vocables.size(), is(VOCABLES.size()));
    }

    @Test
    public void getVocableById() throws InterruptedException {
        mDatabase.subjectDao().insertAll(SUBJECTS);
        mDatabase.unitDao().insertAll(UNITS);

        mVocableDao.insertAll(VOCABLES);

        VocableEntity vocable = LiveDataTestUtil.getValue(mVocableDao.loadById(VOCABLE_ENTITY.getId()));

        assertThat(vocable.getId(), is(VOCABLE_ENTITY.getId()));
        assertThat(vocable.getKey(), is(VOCABLE_ENTITY.getKey()));
        assertThat(vocable.getValuesAsList(), is(VOCABLE_ENTITY.getValuesAsList()));
        assertThat(vocable.getValues(), is(VOCABLE_ENTITY.getValues()));
        assertThat(vocable.getHelper(), is(VOCABLE_ENTITY.getHelper()));
        assertThat(vocable.getLevel(), is(VOCABLE_ENTITY.getLevel()));
        assertThat(vocable.getUnitId(), is(VOCABLE_ENTITY.getUnitId()));
    }

    @Test
    public void deleteVocableByUnitId() throws InterruptedException {
        mDatabase.subjectDao().insertAll(SUBJECTS);
        mDatabase.unitDao().insertAll(UNITS);

        mVocableDao.insertAll(VOCABLES);

        List<VocableEntity> vocables = LiveDataTestUtil.getValue(mVocableDao.loadAllByUnitId(UNIT_ENTITY.getId()));
        int vocablesSize = VOCABLES.stream().filter(v -> v.getUnitId().equals(UNIT_ENTITY.getId())).collect(Collectors.toList()).size();
        assertThat(vocables.size(), is(vocablesSize));

        mVocableDao.deleteAllByUnitId(UNIT_ENTITY.getId());
        List<VocableEntity> vocables02 = LiveDataTestUtil.getValue(mVocableDao.loadAllByUnitId(UNIT_ENTITY.getId()));
        assertTrue(vocables02.isEmpty());
    }
}
