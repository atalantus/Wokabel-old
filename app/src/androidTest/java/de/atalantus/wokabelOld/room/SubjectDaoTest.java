package de.atalantus.wokabelOld.room;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import de.atalantus.wokabelOld.LiveDataTestUtil;
import de.atalantus.wokabelOld.services.room.WokabelDatabase;
import de.atalantus.wokabelOld.services.room.daos.SubjectDao;
import de.atalantus.wokabelOld.services.room.entities.SubjectEntity;

import static de.atalantus.wokabelOld.room.TestData.SUBJECTS;
import static de.atalantus.wokabelOld.room.TestData.SUBJECT_ENTITY;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class SubjectDaoTest {

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    private WokabelDatabase mDatabase;
    private SubjectDao subjectDao;

    @Before
    public void initDb() throws Exception
    {
        // using an in-memory database because the information stored here disappears when the
        // process is killed
        mDatabase = Room.inMemoryDatabaseBuilder(ApplicationProvider.getApplicationContext(),
                WokabelDatabase.class)
                // allowing main thread queries, just for testing
                .allowMainThreadQueries()
                .build();

        subjectDao = mDatabase.subjectDao();
    }

    @After
    public void closeDb() throws Exception {
        mDatabase.close();
    }

    @Test
    public void getSubjectsWhenNoSubjectsInserted() throws InterruptedException {
        List<SubjectEntity> subjects = LiveDataTestUtil.getValue(subjectDao.loadAll());

        assertTrue(subjects.isEmpty());
    }

    @Test
    public void getSubjectsAfterInserted() throws InterruptedException {
        subjectDao.insertAll(SUBJECTS);

        List<SubjectEntity> subjects = LiveDataTestUtil.getValue(subjectDao.loadAll());

        assertThat(subjects.size(), is(SUBJECTS.size()));
    }

    @Test
    public void getSubjectById() throws InterruptedException {
        subjectDao.insertAll(SUBJECTS);

        SubjectEntity subject = LiveDataTestUtil.getValue(subjectDao.loadById
                (SUBJECT_ENTITY.getId()));

        assertThat(subject.getId(), is(SUBJECT_ENTITY.getId()));
        assertThat(subject.getName(), is(SUBJECT_ENTITY.getName()));
        assertThat(subject.getIconName(), is(SUBJECT_ENTITY.getIconName()));
    }
}
