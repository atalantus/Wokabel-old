package de.atalantus.wokabelOld.room;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import de.atalantus.wokabelOld.LiveDataTestUtil;
import de.atalantus.wokabelOld.services.room.WokabelDatabase;
import de.atalantus.wokabelOld.services.room.daos.UnitDao;
import de.atalantus.wokabelOld.services.room.entities.UnitEntity;

import static de.atalantus.wokabelOld.room.TestData.SUBJECTS;
import static de.atalantus.wokabelOld.room.TestData.UNITS;
import static de.atalantus.wokabelOld.room.TestData.UNIT_ENTITY;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class UnitDaoTest {

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    private WokabelDatabase mDatabase;
    private UnitDao unitDao;

    @Before
    public void initDb() throws Exception
    {
        // using an in-memory database because the information stored here disappears when the
        // process is killed
        mDatabase = Room.inMemoryDatabaseBuilder(ApplicationProvider.getApplicationContext(),
                WokabelDatabase.class)
                // allowing main thread queries, just for testing
                .allowMainThreadQueries()
                .build();

        unitDao = mDatabase.unitDao();
    }

    @After
    public void closeDb() throws Exception {
        mDatabase.close();
    }

    @Test
    public void getUnitsWhenNoUnitsInserted() throws InterruptedException {
        List<UnitEntity> units = LiveDataTestUtil.getValue(unitDao.loadAll());

        assertTrue(units.isEmpty());
    }

    @Test
    public void getUnitsAfterInserted() throws InterruptedException {
        mDatabase.subjectDao().insertAll(SUBJECTS);

        mDatabase.unitDao().insertAll(UNITS);

        List<UnitEntity> untis = LiveDataTestUtil.getValue(unitDao.loadAll());

        assertThat(untis.size(), is(UNITS.size()));
    }

    @Test
    public void getUnitById() throws InterruptedException {
        mDatabase.subjectDao().insertAll(SUBJECTS);

        mDatabase.unitDao().insertAll(UNITS);

        UnitEntity unit = LiveDataTestUtil.getValue(unitDao.loadById
                (UNIT_ENTITY.getId()));

        assertThat(unit.getId(), is(UNIT_ENTITY.getId()));
        assertThat(unit.getName(), is(UNIT_ENTITY.getName()));
        assertThat(unit.getSubjectId(), is(UNIT_ENTITY.getSubjectId()));
    }
}
