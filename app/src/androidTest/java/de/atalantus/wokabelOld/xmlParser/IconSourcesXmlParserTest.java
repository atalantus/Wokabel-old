package de.atalantus.wokabelOld.xmlParser;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;

import de.atalantus.wokabelOld.models.IconGroup;
import de.atalantus.wokabelOld.services.XmlParser.IconSourcesXmlParser;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class IconSourcesXmlParserTest {

    @Test
    public void getSubjectIconGroups() throws IOException, XmlPullParserException {
        Context appContext = ApplicationProvider.getApplicationContext();

        IconSourcesXmlParser parser = new IconSourcesXmlParser();
        List<IconGroup> groups = parser.parse(appContext.getAssets().open("icon_sources.xml"), IconSourcesXmlParser.IconGroups.SUBJECT, "icon_flag_brazil");

        assertThat(groups.size(), is(2));

        IconGroup flags = groups.get(0);
        assertThat(flags.getName(), is("icon_flags_title"));
        assertThat(flags.getIcons().size(), is(17));
        assertTrue(flags.getIcons().get(0).isSelected());

        IconGroup other = groups.get(1);
        assertThat(other.getName(), is("icon_other_title"));
        assertThat(other.getIcons().size(), is(10));
        assertFalse(other.getIcons().get(0).isSelected());
    }

    @Test
    public void getProfileIconGroups() throws IOException, XmlPullParserException {
        Context appContext = ApplicationProvider.getApplicationContext();

        IconSourcesXmlParser parser = new IconSourcesXmlParser();
        List<IconGroup> groups = parser.parse(appContext.getAssets().open("icon_sources.xml"), IconSourcesXmlParser.IconGroups.PROFILE, "avatar_crown_green");

        assertThat(groups.size(), is(1));

        IconGroup avatars = groups.get(0);
        assertThat(avatars.getName(), is("icon_avatars_title"));
        assertThat(avatars.getIcons().size(), is(3));
        assertFalse(avatars.getIcons().get(0).isSelected());
        assertTrue(avatars.getIcons().get(1).isSelected());
    }
}
