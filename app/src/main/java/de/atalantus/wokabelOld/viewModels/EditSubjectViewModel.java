package de.atalantus.wokabelOld.viewModels;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;

import de.atalantus.wokabelOld.WokabelOldApplication;
import de.atalantus.wokabelOld.services.room.entities.SubjectEntity;
import de.atalantus.wokabelOld.services.room.repositories.SubjectRepository;

public class EditSubjectViewModel extends AndroidViewModel {
    private SubjectEntity editedSubject;
    private boolean isNewSubject;
    private String packageName;

    private SubjectRepository subjectRepository;

    public EditSubjectViewModel(Application application) {
        super(application);
        subjectRepository = ((WokabelOldApplication) application).getSubjectRepository();

        packageName = getApplication().getPackageName();
    }

    public void createEditSubject(String id, String name, String iconName) {
        if (id == null) {
            isNewSubject = true;
            editedSubject = new SubjectEntity(name, iconName);
        } else {
            isNewSubject = false;
            editedSubject = new SubjectEntity(id, name, iconName);
        }
    }

    public void saveSubject() {
        Log.d("EditSubjectViewModel", "editedSubjectName: " + editedSubject.getName());
        Log.d("EditSubjectViewModel", "editedSubjectIconName: " + editedSubject.getIconName());
/*
        if (isNewSubject) {
            subjectRepository.createSubject(editedSubject);
        } else {
            subjectRepository.updateSubject(editedSubject);
        }

 */
    editedSubject.setName("WhoopWhoop");
    }

    public String getSubjectName() {
        return editedSubject.getName();
    }

    public void setSubjectName(String name) {
        editedSubject.setName(name);
    }

    public String getSubjectIconName() {
        return editedSubject.getIconName();
    }

    public void setSubjectIconName(String name) {
        editedSubject.setIconName(name);
    }

    public int getSubjectIconId() {
        return getApplication().getResources().getIdentifier(editedSubject.getIconName(), "drawable", packageName);
    }

    public void setSubjectIconId(int resId) {
        editedSubject.setIconName(getApplication().getResources().getResourceEntryName(resId));
    }

    public boolean isNewSubject() {
        return isNewSubject;
    }

    public SubjectEntity getEditedSubject() {
        return editedSubject;
    }
}
