package de.atalantus.wokabelOld.services.room.repositories;

import de.atalantus.wokabelOld.services.room.WokabelDatabase;

public class VocableRepository {
    private static VocableRepository instance;
    private final WokabelDatabase database;

    private VocableRepository(final WokabelDatabase database) {
        this.database = database;
    }

    public static VocableRepository getInstance(final WokabelDatabase database) {
        if (instance == null) {
            synchronized (VocableRepository.class) {
                if (instance == null) {
                    instance = new VocableRepository(database);
                }
            }
        }
        return instance;
    }
}
