package de.atalantus.wokabelOld.services.room.repositories;

import androidx.lifecycle.LiveData;

import java.util.List;

import de.atalantus.wokabelOld.services.room.WokabelDatabase;
import de.atalantus.wokabelOld.services.room.entities.SubjectEntity;

public class SubjectRepository {
    private static SubjectRepository instance;

    private final WokabelDatabase database;

    private SubjectRepository(final WokabelDatabase database) {
        this.database = database;
    }

    public static SubjectRepository getInstance(final WokabelDatabase database) {
        if (instance == null) {
            synchronized (SubjectRepository.class) {
                if (instance == null) {
                    instance = new SubjectRepository(database);
                }
            }
        }
        return instance;
    }

    public LiveData<List<SubjectEntity>> getAllSubjects() {
        return database.subjectDao().loadAll();
    }

    public void deleteSubject(String id) {
        // TODO: get Units of Subject

        // delete all Units' Vocables

        // delete all Subject's Units

        // delete Subject
    }

    public void updateSubject(SubjectEntity subject) {
        database.subjectDao().updateSubject(subject);
    }

    public void createSubject(SubjectEntity subject) {
        database.subjectDao().insertSubject(subject);
    }
}
