package de.atalantus.wokabelOld.services.room.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import de.atalantus.wokabelOld.services.room.entities.SubjectEntity;

@Dao
public interface SubjectDao {
    @Insert(onConflict = OnConflictStrategy.ABORT)
    void insertSubject(SubjectEntity subject);

    @Insert(onConflict = OnConflictStrategy.ABORT)
    void insertSubjects(SubjectEntity... subject);

    @Insert(onConflict = OnConflictStrategy.ABORT)
    void insertAll(List<SubjectEntity> subjects);

    @Update
    void updateSubject(SubjectEntity subject);

    @Query("DELETE FROM subject WHERE id = :id")
    void deleteSubject(String id);

    @Query("SELECT * FROM subject WHERE id = :id")
    LiveData<SubjectEntity> loadById(String id);

    @Query("SELECT * FROM subject")
    LiveData<List<SubjectEntity>> loadAll();

    @Query("SELECT * FROM subject")
    List<SubjectEntity> loadSubjectsSync();
}
