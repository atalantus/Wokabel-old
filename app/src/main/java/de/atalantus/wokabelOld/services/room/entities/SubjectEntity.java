package de.atalantus.wokabelOld.services.room.entities;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import java.util.UUID;

/**
 * A group of {@link SubjectEntity}s
 */
@Entity(tableName = "subject", indices = {@Index("id")})
public class SubjectEntity {
    @PrimaryKey
    @NonNull
    private String id;
    private String name;
    private String iconName;

    /**
     * Constructor for initiating a NEW {@link SubjectEntity}
     *
     * @param name     The name
     * @param iconName The name of the drawable resource (without .xml)
     */
    @Ignore
    public SubjectEntity(String name, String iconName) {
        this.name = name;
        this.iconName = iconName;
        id = "S" + UUID.randomUUID().toString();
    }

    /**
     * Constructor for initiating an OLD {@link SubjectEntity}
     *
     * @param id       The id
     * @param name     The name
     * @param iconName The name of the drawable resource (without .xml)
     */
    public SubjectEntity(@NonNull String id, String name, String iconName) {
        this.id = id;
        this.name = name;
        this.iconName = iconName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }
}
