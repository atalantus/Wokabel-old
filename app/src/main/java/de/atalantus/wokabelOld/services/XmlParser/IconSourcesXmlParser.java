package de.atalantus.wokabelOld.services.XmlParser;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import de.atalantus.wokabelOld.models.IconGroup;
import de.atalantus.wokabelOld.models.SelectableIcon;

public class IconSourcesXmlParser {
    public enum IconGroups {
        SUBJECT,
        PROFILE
    }

    private static final String ns = null;

    public List<IconGroup> parse(InputStream in, IconGroups group, String selectedSource) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readFeed(parser, group, selectedSource);
        } finally {
            in.close();
        }
    }

    private List<IconGroup> readFeed(XmlPullParser parser, IconGroups group, String selectedSource) throws XmlPullParserException, IOException {
        List<IconGroup> entries = new ArrayList<IconGroup>();

        parser.require(XmlPullParser.START_TAG, ns, "icons");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for right group tag
            if (name.equals("groups") && checkIconGroup(parser, group)) {
                parser.require(XmlPullParser.START_TAG, ns, "groups");
                while (parser.next() != XmlPullParser.END_TAG) {
                    if (parser.getEventType() != XmlPullParser.START_TAG) {
                        continue;
                    }
                    String groupName = parser.getName();

                    if (groupName.equals("group")) {
                        entries.add(readEntry(parser, selectedSource));
                    } else {
                        skip(parser);
                    }
                }
            } else {
                skip(parser);
            }
        }
        return entries;
    }

    private boolean checkIconGroup(XmlPullParser parser, IconGroups group) throws IOException, XmlPullParserException {
        switch (group) {
            case PROFILE:
                return readGroupsClass(parser).equals("profile");
            case SUBJECT:
                return readGroupsClass(parser).equals("subject");
        }
        return false;
    }

    // Parses the contents of an entry. If it encounters a title, summary, or link tag, hands them off
    // to their respective "read" methods for processing. Otherwise, skips the tag.
    private IconGroup readEntry(XmlPullParser parser, String selectedSource) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "group");
        String groupName = null;
        List<SelectableIcon> icons = new ArrayList<>();

        groupName = readGroupName(parser);

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("icon")) {
                SelectableIcon icon = new SelectableIcon(readIcon(parser));
                icon.setSelected(icon.getSource().equals(selectedSource));
                icons.add(icon);
            } else {
                skip(parser);
            }
        }
        return new IconGroup(groupName, icons);
    }

    // Processes group tags in the feed.
    private String readGroupsClass(XmlPullParser parser) throws IOException, XmlPullParserException {
        String className;
        parser.require(XmlPullParser.START_TAG, ns, "groups");
        className = parser.getAttributeValue(null, "class");
        return className;
    }

    // Processes group tags in the feed.
    private String readGroupName(XmlPullParser parser) throws IOException, XmlPullParserException {
        String name;
        parser.require(XmlPullParser.START_TAG, ns, "group");
        name = parser.getAttributeValue(null, "name");
        return name;
    }

    // Processes icon tags in the feed.
    private String readIcon(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "icon");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "icon");
        return title;
    }

    // For the icon tags
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}
