package de.atalantus.wokabelOld.services.room.converters;

import androidx.room.TypeConverter;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;

public class Typeconverter {
    @TypeConverter
    public static String toString(ArrayList<String> list){
        return TextUtils.join(", ", list);
    }

    @TypeConverter
    public static ArrayList<String> fromString(String string){
        return new ArrayList<>(Arrays.asList(string.split(", ")));
    }
}
