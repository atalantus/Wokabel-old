package de.atalantus.wokabelOld.services.room.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import de.atalantus.wokabelOld.services.room.entities.UnitEntity;

@Dao
public interface UnitDao {
    @Insert(onConflict = OnConflictStrategy.ABORT)
    void insertUnit(UnitEntity unit);

    @Insert(onConflict = OnConflictStrategy.ABORT)
    void insertUnits(UnitEntity... units);

    @Insert(onConflict = OnConflictStrategy.ABORT)
    void insertAll(List<UnitEntity> units);

    @Update
    void updateUnit(UnitEntity unit);

    @Query("DELETE FROM unit WHERE id = :id")
    void deleteUnit(String id);

    @Query("DELETE FROM unit WHERE subjectId = :subjectId")
    void deleteAllBySubjectId(String subjectId);

    @Query("SELECT * FROM unit WHERE subjectId = :subjectId")
    LiveData<List<UnitEntity>> loadAllBySubjectId(String subjectId);

    @Query("SELECT * FROM unit")
    LiveData<List<UnitEntity>> loadAll();

    @Query("SELECT * FROM unit WHERE id = :id")
    LiveData<UnitEntity> loadById(String id);
}
