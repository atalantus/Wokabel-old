package de.atalantus.wokabelOld.services.room.entities;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import java.util.UUID;


/**
 * Contains multiple {@link VocableEntity}s
 */
@Entity(tableName = "unit", indices = {@Index("subjectId")}, foreignKeys = @ForeignKey(entity = SubjectEntity.class, parentColumns = "id", childColumns = "subjectId"))
public class UnitEntity {
    @PrimaryKey
    @NonNull
    private String id;
    private String name;
    private String subjectId;

    /**
     * Constructor for initiating a NEW {@link UnitEntity}
     * @param name The name
     * @param subjectId The Subject ID
     */
    @Ignore
    public UnitEntity(String name, String subjectId) {
        this.name = name;
        this.subjectId = subjectId;
        id = "U" + UUID.randomUUID().toString();
    }

    /**
     * Constructor for initiating an OLD {@link UnitEntity}
     * @param name The name
     * @param id The ID
     * @param subjectId The subject's ID
     */
    public UnitEntity(@NonNull String id, String name, String subjectId) {
        this.id = id;
        this.name = name;
        this.subjectId = subjectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) { this.id = id; }

    public String getSubjectId(){
        return subjectId;
    }

    public void setSubjectId(String subjectId) { this.subjectId = subjectId; }
}
