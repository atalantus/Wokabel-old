package de.atalantus.wokabelOld.services.room.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import de.atalantus.wokabelOld.services.room.entities.VocableEntity;

@Dao
public interface VocableDao {
    @Insert(onConflict = OnConflictStrategy.ABORT)
    void insertVocable(VocableEntity vocable);

    @Insert(onConflict = OnConflictStrategy.ABORT)
    void insertVocables(VocableEntity... vocables);

    @Insert(onConflict = OnConflictStrategy.ABORT)
    void insertAll(List<VocableEntity> vocables);

    @Update
    void updateVocable(VocableEntity vocable);

    @Query("DELETE FROM vocable WHERE id = :id")
    void deleteVocable(String id);

    @Query("DELETE FROM vocable WHERE unitId = :unitId")
    void deleteAllByUnitId(String unitId);

    @Query("SELECT * FROM vocable WHERE id = :id")
    LiveData<VocableEntity> loadById(String id);

    @Query("SELECT * FROM vocable")
    LiveData<List<VocableEntity>> loadAll();

    @Query("SELECT * FROM vocable WHERE unitId = :unitId")
    LiveData<List<VocableEntity>> loadAllByUnitId(String unitId);
}
