package de.atalantus.wokabelOld.services.room;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import android.content.Context;
import androidx.annotation.VisibleForTesting;

import de.atalantus.wokabelOld.services.room.converters.Typeconverter;
import de.atalantus.wokabelOld.services.room.daos.SubjectDao;
import de.atalantus.wokabelOld.services.room.daos.UnitDao;
import de.atalantus.wokabelOld.services.room.daos.VocableDao;
import de.atalantus.wokabelOld.services.room.entities.*;

@Database(entities = {SubjectEntity.class, UnitEntity.class, VocableEntity.class}, version = 1, exportSchema = false)
@TypeConverters(Typeconverter.class)
public abstract class WokabelDatabase extends RoomDatabase {

    private static WokabelDatabase instance;

    @VisibleForTesting
    public static final String DATABASE_NAME = "wokabelDatabase";

    private final MutableLiveData<Boolean> isDatabaseCreated = new MutableLiveData<>();

    public abstract SubjectDao subjectDao();
    public abstract UnitDao unitDao();
    public abstract VocableDao vocableDao();

    public static WokabelDatabase getInstance(final Context context) {

        if(instance == null){
            synchronized (WokabelDatabase.class){
                if(instance == null){
                    instance = Room.databaseBuilder(context.getApplicationContext(),
                            WokabelDatabase.class, DATABASE_NAME)
                            .build();
                    instance.updateDatabaseCreated(context.getApplicationContext());
                }
            }
        }
        return instance;
    }

    /**
     * Check whether the database already exists and expose it via {@link #getDatabaseCreated()}
     */
    private void updateDatabaseCreated(final Context context) {
        if (context.getDatabasePath(DATABASE_NAME).exists()) {
            setDatabaseCreated();
        }
    }

    private void setDatabaseCreated(){
        isDatabaseCreated.postValue(true);
    }

    /**
     * Checks whether database is created or not
     * @return True if the database has been created
     */
    public LiveData<Boolean> getDatabaseCreated() {
        return isDatabaseCreated;
    }
}
