package de.atalantus.wokabelOld.services.room.entities;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;


/**
 * A vocable
 */
@Entity(tableName = "vocable", indices = {@Index("unitId")}, foreignKeys = @ForeignKey(entity = UnitEntity.class, parentColumns = "id", childColumns = "unitId"))
public class VocableEntity {
    @PrimaryKey
    @NonNull
    private String id;
    private String key;
    private String values;
    private String helper;
    private int level;
    private String unitId;

    /**
     * Constructor for initiating a NEW {@link VocableEntity}
     * @param key The query
     * @param values The different answer possibilities
     * @param helper The tip if the User doesn't know the answer
     * @param unitId The id of the Unit the Vocable is assigned to
     */
    @Ignore
    public VocableEntity(String key, ArrayList<String> values, String helper, String unitId) {
        this.key = key;
        this.values = TextUtils.join(", ", values);
        this.helper = helper;
        level = 0;
        this.unitId = unitId;
        id = "V" + UUID.randomUUID().toString();
    }

    @Ignore
    public VocableEntity(String key, String values, String helper, String unitId) {
        this.key = key;
        this.values = values;
        this.helper = helper;
        level = 0;
        this.unitId = unitId;
        id = "V" + UUID.randomUUID().toString();
    }

    /**
     * Constructor for initiating an OLD {@link VocableEntity}
     * @param id The ID
     * @param key The query
     * @param values The different answer possibilities
     * @param helper The tipp if the User doesn't know the answer
     * @param level The level
     * @param unitId The ID of the Unit the Vocable is assigned to
     */
    public VocableEntity(@NonNull String id, String key, String values, String helper, int level, String unitId) {
        this.key = key;
        this.values = values;
        this.helper = helper;
        this.id = id;
        this.level = level;
        this.unitId = unitId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(@NonNull String key) {
        this.key = key;
    }

    public ArrayList<String> getValuesAsList() {
        return new ArrayList<>(Arrays.asList(values.split(", ")));
    }

    public String getValues(){
        return values;
    }

    public void setValues(ArrayList<String> values) {
        this.values = TextUtils.join(", ", values);
    }

    public void setValues(String values){
        this.values = values;
    }

    public String getHelper() {
        return helper;
    }

    public void setHelper(String helper) {
        this.helper = helper;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) { this.id = id; }

    public String getUnitId(){
        return unitId;
    }

    public void setUnitId(String unitId) { this.unitId = unitId; }
}
