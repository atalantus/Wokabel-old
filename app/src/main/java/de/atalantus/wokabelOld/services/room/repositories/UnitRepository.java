package de.atalantus.wokabelOld.services.room.repositories;

import de.atalantus.wokabelOld.services.room.WokabelDatabase;

public class UnitRepository {
    private static UnitRepository instance;
    private final WokabelDatabase database;

    private UnitRepository(final WokabelDatabase database) {
        this.database = database;
    }

    public static UnitRepository getInstance(final WokabelDatabase database) {
        if (instance == null) {
            synchronized (UnitRepository.class) {
                if (instance == null) {
                    instance = new UnitRepository(database);
                }
            }
        }
        return instance;
    }
}
