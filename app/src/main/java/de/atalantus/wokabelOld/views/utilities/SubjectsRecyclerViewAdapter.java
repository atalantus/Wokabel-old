package de.atalantus.wokabelOld.views.utilities;

import android.content.Context;
import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.atalantus.wokabelOld.R;
import de.atalantus.wokabelOld.services.room.entities.SubjectEntity;
import de.atalantus.wokabelOld.views.fragments.SubjectSelectFragment.OnListFragmentInteractionListener;

/**
 * {@link RecyclerView.Adapter} that can display a {@link SubjectEntity} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 */
public class SubjectsRecyclerViewAdapter extends RecyclerView.Adapter<SubjectsRecyclerViewAdapter.ViewHolder> {

    private List<SubjectEntity> subjects;
    private Resources resources;
    private final OnListFragmentInteractionListener listener;

    public SubjectsRecyclerViewAdapter(Context context, OnListFragmentInteractionListener listener, List<SubjectEntity> subjects) {
        this.subjects = subjects;
        resources = context.getResources();
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_subjects_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        SubjectEntity subject = holder.subjectEntity = subjects.get(position);
        int iconResId = resources.getIdentifier(subject.getIconName(), "drawable", "de.atalantus.wokabel");

        holder.icon.setImageResource(iconResId);
        holder.name.setText(subject.getName());
        holder.foregroundLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != listener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    listener.onSubjectSelected(holder.subjectEntity);
                }
            }
        });
        holder.editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != listener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected
                    // to be edited.
                    listener.onSubjectEditSelected(holder.subjectEntity);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return subjects.size();
    }

    public void removeItem(int position) {
        subjects.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreItem(SubjectEntity subject, int position) {
        subjects.add(position, subject);
        // notify item added by position
        notifyItemInserted(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final ImageView icon;
        public final TextView name;
        public final ImageButton editBtn;
        public final FrameLayout foregroundLayout;
        public final ConstraintLayout backgroundLayout;
        public SubjectEntity subjectEntity;

        public ViewHolder(View view) {
            super(view);
            icon = view.findViewById(R.id.icon);
            name = view.findViewById(R.id.name);
            editBtn = view.findViewById(R.id.edit_btn);
            foregroundLayout = view.findViewById(R.id.view_foreground);
            backgroundLayout = view.findViewById(R.id.view_background);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + name.getText() + "'";
        }
    }
}
