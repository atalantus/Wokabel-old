package de.atalantus.wokabelOld.views.fragments;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import de.atalantus.wokabelOld.R;
import de.atalantus.wokabelOld.services.room.entities.SubjectEntity;
import de.atalantus.wokabelOld.views.utilities.SubjectsRecyclerTouchHelper;
import de.atalantus.wokabelOld.views.utilities.SubjectsRecyclerViewAdapter;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class SubjectSelectFragment extends Fragment implements SubjectsRecyclerTouchHelper.RecyclerItemTouchHelperListener {

    private List<SubjectEntity> cachedSubjects;

    private SubjectsRecyclerViewAdapter adapter;
    private RecyclerView recyclerView;
    private OnListFragmentInteractionListener listener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SubjectSelectFragment() {
    }

    public static SubjectSelectFragment newInstance() {
        return new SubjectSelectFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // TODO: Beispiel Daten entfernen
        cachedSubjects = new ArrayList<>();
        cachedSubjects.add(new SubjectEntity("Englisch", "icon_flag_united_kingdom"));
        cachedSubjects.add(new SubjectEntity("Spanisch", "icon_flag_spain"));
        cachedSubjects.add(new SubjectEntity("Geschichte", "icon_earth_globe"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_subjects, container, false);

        ((AppCompatActivity)getActivity()).setSupportActionBar((Toolbar) view.findViewById(R.id.toolbar));

        Context context = view.getContext();
        adapter = new SubjectsRecyclerViewAdapter(context, listener, cachedSubjects);
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);

        // adding item touch helper
        // only ItemTouchHelper.LEFT added to detect Right to Left swipe
        // if you want both Right -> Left and Left -> Right
        // add pass ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT as param
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new SubjectsRecyclerTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);

        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            listener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    /**
     * callback when recycler view is swiped
     * item will be removed on swiped
     * undo option will be provided in snackbar to restore the item
     */
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof SubjectsRecyclerViewAdapter.ViewHolder) {
            // get the removed item name to display it in snack bar
            String name = cachedSubjects.get(viewHolder.getAdapterPosition()).getName();

            // backup of removed item for undo purpose
            final SubjectEntity removedItem = cachedSubjects.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            adapter.removeItem(viewHolder.getAdapterPosition());

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            String msg = getResources().getString(R.string.delete_subject_message);
            builder.setMessage(String.format(msg, name))
                    .setTitle(R.string.are_you_sure);
            builder.setPositiveButton(R.string.delete, (dialog, which) -> deleteSubject(removedItem))
            .setNegativeButton(android.R.string.cancel, (dialog, id) -> {
                // User cancelled the dialog
                adapter.restoreItem(removedItem, deletedIndex);
            });
            builder.show();
        }
    }

    private void deleteSubject(SubjectEntity subject) {
        // TODO: Delete Subject, Units and Vocables from database
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onSubjectSelected(SubjectEntity subject);
        void onSubjectEditSelected(SubjectEntity subject);
    }
}
