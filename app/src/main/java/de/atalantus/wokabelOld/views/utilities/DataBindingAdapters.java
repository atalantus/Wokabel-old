package de.atalantus.wokabelOld.views.utilities;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

public class DataBindingAdapters {
    @BindingAdapter("resourceName")
    public static void setImageResource(ImageView imageView, String resourceName) {
        String packageName = imageView.getContext().getPackageName();
        int resId = imageView.getContext().getResources().getIdentifier(resourceName, "drawable", packageName);
        imageView.setImageResource(resId);
    }
}
