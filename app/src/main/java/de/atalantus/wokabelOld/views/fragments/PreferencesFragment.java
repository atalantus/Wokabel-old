package de.atalantus.wokabelOld.views.fragments;

import android.os.Bundle;
import androidx.preference.PreferenceFragmentCompat;

import de.atalantus.wokabelOld.R;

public class PreferencesFragment extends PreferenceFragmentCompat {

    public static PreferencesFragment newInstance() {
        return new PreferencesFragment();
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        // Load the preferences from an XML resource
        setPreferencesFromResource(R.xml.preferences, rootKey);
    }
}
