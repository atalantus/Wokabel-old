package de.atalantus.wokabelOld.views.activities;

import android.content.DialogInterface;
import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import de.atalantus.wokabelOld.R;
import de.atalantus.wokabelOld.WokabelOldApplication;
import de.atalantus.wokabelOld.services.settings.Settings;

/**
 * Main activity class for the Welcome Screen
 */
public class Welcome extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
    }

    public void startBtn(View view) {
        final EditText forenameInput = findViewById(R.id.forename_input);
        String forename = forenameInput.getText().toString();

        if (!forename.isEmpty()) {
            Settings settings = ((WokabelOldApplication) getApplication()).getSharedSettings();
            settings.setString("username", forename);
            Intent intent = new Intent(this, Home.class);
            finish();
            startActivity(intent);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.welcome_invalid_name_msg)
                    .setTitle(R.string.oops_title);
            builder.setPositiveButton(getResources().getText(android.R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    forenameInput.requestFocus();
                }
            });
            builder.show();
        }
    }
}
