package de.atalantus.wokabelOld.views.activities;

import android.app.SearchManager;

import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import de.atalantus.wokabelOld.R;
import de.atalantus.wokabelOld.services.room.entities.SubjectEntity;
import de.atalantus.wokabelOld.viewModels.HomeViewModel;
import de.atalantus.wokabelOld.views.fragments.ProfileFragment;
import de.atalantus.wokabelOld.views.fragments.SettingsFragment;
import de.atalantus.wokabelOld.views.fragments.SubjectSelectFragment;

/**
 * Main activity class for the Home Screen
 */
public class Home extends AppCompatActivity implements SubjectSelectFragment.OnListFragmentInteractionListener {
    private Fragment selectedFragment = null;
    private ProfileFragment profileFragment;
    private SubjectSelectFragment subjectSelectFragment;
    private SettingsFragment settingsFragment;

    private HomeViewModel viewModel;
    private ConstraintLayout parentLayout;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case R.id.navigation_profile:
                    setTitle(R.string.title_profile);
                    selectedFragment = profileFragment;
                    break;
                case R.id.navigation_subjects:
                    setTitle(R.string.title_subjects);
                    selectedFragment = subjectSelectFragment;
                    break;
                case R.id.navigation_settings:
                    setTitle(R.string.title_settings);
                    selectedFragment = settingsFragment;
                    break;
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_frame, selectedFragment);
            transaction.commit();

            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        viewModel = ViewModelProviders.of(this).get(HomeViewModel.class);

        parentLayout = findViewById(R.id.parent_layout);

        profileFragment = ProfileFragment.newInstance();
        subjectSelectFragment = SubjectSelectFragment.newInstance();
        settingsFragment = SettingsFragment.newInstance();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_subjects);
    }

    public HomeViewModel getViewModel() {
        return viewModel;
    }

    public ConstraintLayout getParentLayout() {
        return parentLayout;
    }

    public void createSubject(View view) {
        Intent appIntent = new Intent(this, EditSubject.class);

        // pass subject's values
        appIntent.putExtra("subjectId", (String) null);
        appIntent.putExtra("subjectName", "New Subject");
        appIntent.putExtra("subjectIconName", "icon_flag_united_kingdom");

        startActivity(appIntent);
    }

    @Override
    public void onSubjectSelected(SubjectEntity subject) {
        // TODO: Weiter zu UnitSelect
    }

    /**
     * Edit selected subject values in popup activity
     *
     * @param subject - subject to edit
     */
    @Override
    public void onSubjectEditSelected(SubjectEntity subject) {
        Intent appIntent = new Intent(this, EditSubject.class);

        // pass subject's values
        appIntent.putExtra("subjectId", subject.getId());
        appIntent.putExtra("subjectName", subject.getName());
        appIntent.putExtra("subjectIconName", subject.getIconName());

        startActivity(appIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (selectedFragment.equals(subjectSelectFragment)) {
            getMenuInflater().inflate(R.menu.toolbar_subject_select, menu);

            // Associate searchable configuration with the SearchView
            SearchManager searchManager =
                    (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView =
                    (SearchView) menu.findItem(R.id.action_search).getActionView();
            searchView.setSearchableInfo(
                    searchManager.getSearchableInfo(getComponentName()));
        } else
            getMenuInflater().inflate(R.menu.toolbar_empty, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}
