package de.atalantus.wokabelOld.views.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import de.atalantus.wokabelOld.R;
import de.atalantus.wokabelOld.models.SelectableIcon;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SelectableIconFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SelectableIconFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SelectableIconFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_ICON = "icon";

    private SelectableIcon icon;

    private OnFragmentInteractionListener mListener;

    public SelectableIconFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param icon The icon
     * @return A new instance of fragment SelectableIconFragment.
     */
    public static SelectableIconFragment newInstance(SelectableIcon icon) {
        SelectableIconFragment fragment = new SelectableIconFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_ICON, icon);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            icon = getArguments().getParcelable(ARG_ICON);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_selectable_icon, container, false);

        ImageView iconView = view.findViewById(R.id.icon);
        int resId = getContext().getResources().getIdentifier(icon.getSource(), "drawable", getContext().getPackageName());
        iconView.setImageResource(resId);

        Button button = view.findViewById(R.id.button);
        button.setOnClickListener(this::selectIcon);

        return view;
    }

    public void selectIcon(View view) {
        if (mListener != null)
            mListener.OnIconSelected(icon);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void OnIconSelected(SelectableIcon selectedIcon);
    }
}
