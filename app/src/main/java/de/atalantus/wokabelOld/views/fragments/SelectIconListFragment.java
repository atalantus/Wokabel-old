package de.atalantus.wokabelOld.views.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;

import de.atalantus.wokabelOld.R;
import de.atalantus.wokabelOld.models.IconGroup;
import de.atalantus.wokabelOld.services.XmlParser.IconSourcesXmlParser;

public class SelectIconListFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_ICON_GROUPS_TYPE = "iconGroupType";
    private static final String ARG_SELECTED_ICON_SOURCE = "selectedIconSource";

    private IconSourcesXmlParser.IconGroups iconGroupsType;
    private String selectedIconSource;
    private List<IconGroup> iconGroups;

    public SelectIconListFragment() {
        // Required empty public constructor
    }

    public static SelectIconListFragment newInstance(IconSourcesXmlParser.IconGroups groups, String selectedIconSource) {
        SelectIconListFragment fragment = new SelectIconListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_ICON_GROUPS_TYPE, groups);
        args.putString(ARG_SELECTED_ICON_SOURCE, selectedIconSource);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.iconGroupsType = (IconSourcesXmlParser.IconGroups) getArguments().getSerializable(ARG_ICON_GROUPS_TYPE);
            this.selectedIconSource = getArguments().getString(ARG_SELECTED_ICON_SOURCE);
        }

        try {
            iconGroups = new IconSourcesXmlParser().parse(getContext().getAssets().open("icon_sources.xml"), iconGroupsType, selectedIconSource);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_select_icon_list, container, false);

        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        for (int i = 0; i < iconGroups.size(); i++) {
            IconGroupFragment iconGroupFragment = IconGroupFragment.newInstance(iconGroups.get(i));
            fragmentTransaction.add(R.id.icon_groups_parent, iconGroupFragment);
        }

        fragmentTransaction.commit();

        return view;
    }
}
