package de.atalantus.wokabelOld.views.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.atalantus.wokabelOld.R;
import de.atalantus.wokabelOld.models.IconGroup;

public class IconGroupFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_ICON_GROUP = "iconGroup";

    private IconGroup iconGroup;
    private SelectableIconFragment[] iconFragments;

    public IconGroupFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param group The icon group
     * @return A new instance of fragment IconGroupFragment.
     */
    public static IconGroupFragment newInstance(IconGroup group) {
        IconGroupFragment fragment = new IconGroupFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_ICON_GROUP, group);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            this.iconGroup = getArguments().getParcelable(ARG_ICON_GROUP);
        }

        iconFragments = new SelectableIconFragment[iconGroup.getIcons().size()];
        for (int i = 0; i < iconFragments.length; i++) {
            iconFragments[i] = SelectableIconFragment.newInstance(iconGroup.getIcons().get(i));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_icon_group, container, false);

        TextView name = view.findViewById(R.id.group_name);
        int nameId = getResources().getIdentifier(iconGroup.getName(), "string", getContext().getPackageName());
        name.setText(getString(nameId));

        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        for (int i = 0; i < iconFragments.length; i++) {
            fragmentTransaction.add(R.id.icon_grid, iconFragments[i]);
        }

        fragmentTransaction.commit();

        return view;
    }
}
