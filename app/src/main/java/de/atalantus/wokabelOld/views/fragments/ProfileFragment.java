package de.atalantus.wokabelOld.views.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.atalantus.wokabelOld.R;
import de.atalantus.wokabelOld.WokabelOldApplication;
import de.atalantus.wokabelOld.services.settings.Settings;

public class ProfileFragment extends Fragment {

    private Settings settings;
    private TextView username;
    private TextView userXp;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        settings = ((WokabelOldApplication) getActivity().getApplication()).getSharedSettings();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        ((AppCompatActivity)getActivity()).setSupportActionBar((Toolbar) view.findViewById(R.id.toolbar));

        username = view.findViewById(R.id.display_username);
        userXp = view.findViewById(R.id.display_xp);

        displayUserData();

        return view;
    }

    private void displayUserData() {
        username.setText(settings.getString("username"));
        String xp = Integer.toString(settings.getInt("user_xp"));
        userXp.setText(xp);
    }
}
