package de.atalantus.wokabelOld.views.activities;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import de.atalantus.wokabelOld.R;
import de.atalantus.wokabelOld.databinding.ActivityEditSubjectBinding;
import de.atalantus.wokabelOld.models.SelectableIcon;
import de.atalantus.wokabelOld.services.XmlParser.IconSourcesXmlParser;
import de.atalantus.wokabelOld.viewModels.EditSubjectViewModel;
import de.atalantus.wokabelOld.views.fragments.SelectIconListFragment;
import de.atalantus.wokabelOld.views.fragments.SelectableIconFragment;

public class EditSubject extends AppCompatActivity implements SelectableIconFragment.OnFragmentInteractionListener {

    private ImageView selectedIconView;
    private SelectIconListFragment iconListFragment;

    private EditSubjectViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set content view through activity's data binding class
        ActivityEditSubjectBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_subject);

        // get activity specific view model
        viewModel = ViewModelProviders.of(this).get(EditSubjectViewModel.class);

        // set view model for data binding
        binding.setViewmodel(viewModel);

        // get passed through arguments
        Intent intent = getIntent();
        viewModel.createEditSubject(intent.getStringExtra("subjectId"), intent.getStringExtra("subjectName"), intent.getStringExtra("subjectIconName"));

        // set title based on passed arguments in intent
        setTitle(viewModel.isNewSubject() ? getResources().getString(R.string.create_subject_title) : getResources().getString(R.string.edit_subject_title));

        // reference the icon view
        selectedIconView = findViewById(R.id.current_icon);

        // instantiate icon list fragment
        iconListFragment = SelectIconListFragment.newInstance(IconSourcesXmlParser.IconGroups.SUBJECT, viewModel.getSubjectIconName());

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.icon_select_list, iconListFragment);
        fragmentTransaction.commit();

        this.setFinishOnTouchOutside(false);
    }

    public void cancel(View view) {
        finish();
    }

    public void saveChanges(View view) {
        viewModel.saveSubject();
        //finish();
    }

    @Override
    public void OnIconSelected(SelectableIcon selectedIcon) {
        viewModel.setSubjectIconName(selectedIcon.getSource());
    }
}
