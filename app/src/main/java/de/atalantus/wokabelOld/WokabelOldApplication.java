package de.atalantus.wokabelOld;

import android.app.Application;
import android.content.Context;

import de.atalantus.wokabelOld.services.room.WokabelDatabase;
import de.atalantus.wokabelOld.services.room.repositories.SubjectRepository;
import de.atalantus.wokabelOld.services.room.repositories.UnitRepository;
import de.atalantus.wokabelOld.services.room.repositories.VocableRepository;
import de.atalantus.wokabelOld.services.settings.Settings;

public class WokabelOldApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public Settings getSharedSettings() { return Settings.getInstance(this); }

    public Context getAppContext() { return getApplicationContext(); }

    public WokabelDatabase getDatabase() { return WokabelDatabase.getInstance(this); }

    public SubjectRepository getSubjectRepository() { return SubjectRepository.getInstance(getDatabase()); }
    public UnitRepository getUnitRepository() { return UnitRepository.getInstance(getDatabase()); }
    public VocableRepository getVocableRepository() { return VocableRepository.getInstance(getDatabase()); }
}
