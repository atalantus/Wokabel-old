package de.atalantus.wokabelOld.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class IconGroup implements Parcelable {

    private String name;
    private List<SelectableIcon> icons;

    public IconGroup(String name, List<SelectableIcon> icons) {
        this.name = name;
        this.icons = icons;
    }

    protected IconGroup(Parcel in) {
        name = in.readString();
        icons = in.createTypedArrayList(SelectableIcon.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeTypedList(icons);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<IconGroup> CREATOR = new Creator<IconGroup>() {
        @Override
        public IconGroup createFromParcel(Parcel in) {
            return new IconGroup(in);
        }

        @Override
        public IconGroup[] newArray(int size) {
            return new IconGroup[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SelectableIcon> getIcons() {
        return icons;
    }

    public void setIcons(List<SelectableIcon> icons) {
        this.icons = icons;
    }
}
