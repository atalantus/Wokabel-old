package de.atalantus.wokabelOld.models;

import android.os.Parcel;
import android.os.Parcelable;

public class SelectableIcon implements Parcelable {

    private String source;
    private boolean selected;

    public SelectableIcon(String source) {
        this.source = source;
        selected = false;
    }

    protected SelectableIcon(Parcel in) {
        source = in.readString();
        selected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(source);
        dest.writeByte((byte) (selected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SelectableIcon> CREATOR = new Creator<SelectableIcon>() {
        @Override
        public SelectableIcon createFromParcel(Parcel in) {
            return new SelectableIcon(in);
        }

        @Override
        public SelectableIcon[] newArray(int size) {
            return new SelectableIcon[size];
        }
    };

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
