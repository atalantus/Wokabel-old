## Beschreibung

## Probleme/Fragen/Hilfe

## Checkliste
- [ ] Code auf **Bugs** getestet
- [ ] Code auf [**Konventionen**](https://github.com/atalantus/Wokabel-App/wiki/Dokumentation-%7C-Code-Konventionen) geprüft
- [ ] Code **[kommentiert](http://www.java-doc.de/)**
